const { config } = require("./wdio.shared.conf");

//
// ============
// Capabilities
// ============
// Define your capabilities here. WebdriverIO can run multiple capabilities at the same
// time. Depending on the number of capabilities, WebdriverIO launches several test
// sessions. Within your capabilities you can overwrite the spec and exclude options in
// order to group specific specs to a specific capability.
//
// First, you can define how many instances should be started at the same time. Let's
// say you have 3 different capabilities (Chrome, Firefox, and Safari) and you have
// set maxInstances to 1; wdio will spawn 3 processes. Therefore, if you have 10 spec
// files and you set maxInstances to 10, all spec files will get tested at the same time
// and 30 processes will get spawned. The property handles how many capabilities
// from the same test should run tests.

config.services = config.services.concat([[
    'appium',
    {
        command: 'appium',
        logPath : './logs/appium_logs.txt',
        args: {
            'relaxed-security': true,
        },
    },
]]);
config.port = 4723;

config.capabilities = [
    {
        maxInstances: 1,
        platformName: "iOS",
        platformVersion: "14.3",
        deviceName: "iPhone 11",
        automationName: "XCUITest",
        newCommandTimeout: 240,
        noReset: true,
        connectionRetryTimeout: 90000,
        // autoDismissAlerts: true,
        fullContextList: true,
        includeSafariInWebviews: true,
        eventTimings: true,
        app: "app/ios.app.zip",
    }
];

exports.config = config;
