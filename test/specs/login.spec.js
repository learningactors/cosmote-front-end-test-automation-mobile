import LoginScreen from '../screens/login.screen';

const loginScreen = new LoginScreen();

describe('Login Authentication ', () => {

    it('should login with valid credentials', () => {
     //Valid Login
    loginScreen.getUsername().setValue('standard_user');
    loginScreen.getPassword().setValue('secret_sauce');
    loginScreen.getLoginBtn().click();
     expect(loginScreen.getToggleBtn()).toBeVisible();
    });
});