
class LoginScreen {

    getUsername() {
        return $('~test-Username');
    }

    getPassword() {
        return $('~test-Password');
    }

    getLoginBtn() {
        return $('~test-LOGIN');
    }

    getBiometryBtn() {
        return $('~test-biometry');
    }

    getToggleBtn() {
        return $('~test-Toggle');
    }

    getErrorMessageContainer(){
        return $('~test-Error message');
    }

    getErrorMessage(){
         $('//android.view.ViewGroup[@content-desc="test-Error message"]/android.widget.TextView')
    }
}
export default LoginScreen;
